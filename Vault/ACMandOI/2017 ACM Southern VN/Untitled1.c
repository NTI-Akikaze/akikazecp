#include <iostream>
#include <iomanip>
using namespace std;

class Matrix
{
    private:
        int row;
        int column;
        float matrix[10][10];
    public:
        Matrix()
        {
            row = 1;
            column = 1;
            for(int i = 0; i < row; i++)
                for(int j = 0; j < column; j++)
                    matrix[i][j] = 0;
        }

        friend ostream& operator<<(ostream &m_out, const Matrix &temp)
        {
            for(int i = 0; i < temp.row; i++)
            {
                for(int j = 0; j < temp.column; j++)
                    m_out << setw(4) << temp.matrix[i][j] << " ";
                m_out << endl;
            }
            return m_out;
        }

        friend istream& operator>>(istream &m_in, Matrix &temp)
        {
            cout << "row: ";
            m_in >> temp.row;
            cout << "column: ";
            m_in >> temp.column;

            for(int i = 0; i < temp.row; i++)
                for(int j = 0; j < temp.column; j++)
                {
                    cout << "matrix[" << i << "][" << j << "] = ";
                    m_in >> temp.matrix[i][j];
                }
            return m_in;
        }

        Matrix operator+ (Matrix temp2)
        {
            if(row != temp2.row || column != temp2.column)
            {
                cout << "Can't add two matrixes";
                return temp2;
            }
            else
            {
                Matrix temp3;
                temp3.row = temp2.row;
                temp3.column = temp2.column;

                for(int i = 0; i < row; i++)
                    for(int j = 0; j < column; j++)
                        temp3.matrix[i][j] = matrix[i][j] + temp2.matrix[i][j];
                return temp3;
            }
        }

        Matrix operator- (Matrix temp2)
        {
            if(row != temp2.row || column != temp2.column)
            {
                cout << "Can't subtract two matrixes";
                return temp2;
            }
            else
            {
                Matrix temp3;
                temp3.row = temp2.row;
                temp3.column = temp2.column;

                for(int i = 0; i < row; i++)
                    for(int j = 0; j < column; j++)
                        temp3.matrix[i][j] = matrix[i][j] - temp2.matrix[i][j];
                return temp3;
            }
        }
};

int main()
{
    Matrix a,b,c,d;
	/** ahihi :"> comment duoc viet o ngoai cong nha A2 PTIT :"> **/
    cin >> a >> b;
    c = a + b;
    d = a - b;

    cout << c << endl;
    cout << d << endl;

    return 0;
}
